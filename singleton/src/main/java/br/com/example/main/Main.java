package br.com.example.main;

import br.com.example.singleton.SingletonHelper;

/**
 * Example from:
 *
 */
public class Main {
    public static void main(String[] args) {
        SingletonHelper.getInstance().getUrl();
        System.out.println("");
        SingletonHelper.getInstance().getUrl();

    }
}
