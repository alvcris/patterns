package br.com.example.singleton;

/**
 *
 */
public class SingletonHelper {
    private static SingletonHelper singletonHelper;
    private static String url;

    private SingletonHelper(){
        System.out.println("initializing singleton.");
        System.out.println("Setting url got from property file");

        this.url = getURLFromProperty();

        System.out.println("URL saved: " + url);
    }

    /**
     * Get a singleton instance
     * @return the singleton instance
     */
    public static synchronized SingletonHelper getInstance(){
        if(singletonHelper == null){
            singletonHelper = new SingletonHelper();
        }
        // just to illustrate that singleton will be initialized only once
        else{
            System.out.println("Singleton is already initialized");
            System.out.println("URL is: " + singletonHelper.getUrl());
        }
        return singletonHelper;
    }

    /**
     * Get url from the current instance
     * @return url
     */
    public String getUrl(){
        return this.url;
    }

    /**
     * this method will simulate a property read
     */
    private static String getURLFromProperty(){
        //<Logic to read from property goes here>
        return "http://example.com";

    }
}
