package br.com.example.media.interfaces;

/**
 *
 */
public interface MediaPlayer {
    public void play(String audioType, String fileName);
}
